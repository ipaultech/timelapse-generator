#!/bin/bash
#S3 Upload function
source ./upload.sh
path="/timelapse"

#Env vars
PICS_DELETE=${PICS_DELETE:-false}
OUT_VIDEO_DELETE=${OUT_VIDEO_DELETE:-false}
VIDEO_FRAMERATE=${VIDEO_FRAMERATE:-60}
UPLOAD_VIDEO=${UPLOAD_VIDEO:-false}
UPLOAD_THUMBNAIL=${UPLOAD_THUMBNAIL:-false}
UPLOAD_METADATA=${UPLOAD_METADATA:-false}
S3_Bucket=${S3_Bucket:-false}
S3_Endpoint=${S3_Endpoint:-false}
S3_ACCESS_USER=${S3_ACCESS_USER:-false}
S3_ACCESS_KEY=${S3_ACCESS_KEY:-false}
INFORM_AFTER_UPLOAD=${INFORM_AFTER_UPLOAD:-false}
TG_CHAT_ID=${TG_CHAT_ID:-false}
TG_BOT_TOKEN=${TG_BOT_TOKEN:-false}
CUSTOM_DATE_START=${CUSTOM_DATE_START:-false}
CUSTOM_DATE_STOP=${CUSTOM_DATE_STOP:-false}
METADATA_LOCATION=${METADATA_LOCATION:-false}
THUMBNAIL_TXT=${THUMBNAIL_TXT:-false}


generate_music () {
    min=0
    max=200
    number=$(expr $min + $RANDOM % $max)
    sleepnum=$(echo $number/100 | bc -l)
    sleep $sleepnum
    rnd1=$(ls -1 $path/music/*.mp3 |  shuf | head -1)
    rnd2=$(ls -1 $path/music/*.mp3 |  shuf | head -1)
    rnd3=$(ls -1 $path/music/*.mp3 |  shuf | head -1)
    rnd4=$(ls -1 $path/music/*.mp3 |  shuf | head -1)
    ffmpeg -i "concat:$rnd1|$rnd2|$rnd3|$rnd4" $path/tmpout.mp3

    #Cleanup if wanted
    if [ $MUSIC_TO_USED == 'true' ]; then
        mkdir $path/music/used
        mv $rnd1 $path/music/used/
        mv $rnd2 $path/music/used/
        mv $rnd3 $path/music/used/
        mv $rnd4 $path/music/used/
    fi
}


#Generate a txt file with all images
generate_images_to_process () {
    ls -1tr $path/pics | grep -v $path/tmpfiles.txt > $path/tmpfiles.txt
    awk '$0="/timelapse/pics/"$0' tmpfiles.txt > tmpfiles2.txt
}


generate_date_start () {
    if [ $CUSTOM_DATE_START == 'false' ]; then
        startdate=$(cat $path/start.tmp)
    else
        startdate=$CUSTOM_DATE_START
    fi
    echo $startdate
}


generate_date_end () {
    if [ $CUSTOM_DATE_STOP == 'false' ]; then
        enddate=$(date +"%d.%m.%Y")
    else
        enddate=$CUSTOM_DATE_STOP
    fi
    echo $enddate > $path/start.tmp
    echo $enddate
}


generate_date_text () {
    echo $startdate' - '$enddate
}


#Generate thumbnail image
generate_thumbnail () {
    groundimg=$(head -1 $path/tmpfiles2.txt)
    #Image creation
    convert-im6.q16 $groundimg -radial-blur 1 $path/tmpthumb1.jpg
    convert $path/tmpthumb1.jpg -pointsize 200 -fill yellow -draw "text 430,554 '${THUMBNAIL_TXT}'" -pointsize 200 -draw "text 230,900 '$datetxt'" $path/thumb.jpg
    rm -r $path/tmpthumb1.jpg
    cp $path/thumb.jpg $path/pics/thumb.jpg
    for n in {001..030}; do
        sed  -i '1i /timelapse/pics/thumb.jpg' $path/tmpfiles2.txt
    done
}


generate_video () {
    mencoder -nosound -noskip -oac copy -ovc copy -o "$path/tmpout.avi" -mf fps=$VIDEO_FRAMERATE mf://@$path/tmpfiles2.txt
    ffmpeg -i $path/tmpout.avi -i $path/tmpout.mp3 -c:a aac -b:a 320k -c:v vp9 -b:v 3000K -shortest $path/out.mp4
}


#Send information message to telegram
msg_send () {
    curl -d "chat_id=$TG_CHAT_ID&text=$1" -X POST https://api.telegram.org/bot$TG_BOT_TOKEN/sendMessage
}


upload_video () {
    if [ $UPLOAD_VIDEO == 'true' ]; then
        s3_upload $S3_Bucket$1 $path/out.mp4 $S3_Endpoint $S3_ACCESS_USER $S3_ACCESS_KEY 'video.mp4'
        if [[ $? -ne 0 ]]; then
            msg_send 'S3 video upload was not possible.'
        else
            if [ $INFORM_AFTER_UPLOAD == 'true' ]; then
                msg_send 'Video upload successfull, watch it now!  '$startdate' - '$enddate' https://'$S3_Endpoint'/'$S3_Bucket$1'/video.mp4'
            fi
        fi
    fi
}


upload_thumbnail () {
    if [ $UPLOAD_THUMBNAIL == 'true' ]; then
        s3_upload $S3_Bucket$1 $path/thumb.jpg $S3_Endpoint $S3_ACCESS_USER $S3_ACCESS_KEY 'thumb.jpg'
        if [[ $? -ne 0 ]]; then
            msg_send 'S3 thumbnail upload was not possible.'
        fi
    fi
}

upload_metadata () {
    if [ $UPLOAD_METADATA == 'true' ]; then
        #Generate metadata file
        echo 'START_DATE' >> $path/metadata.txt
        echo $startdate >> $path/metadata.txt
        echo 'END_DATE' >> $path/metadata.txt
        echo $enddate >> $path/metadata.txt
        echo 'DATE_TEXT' >> $path/metadata.txt
        echo $datetxt >> $path/metadata.txt
        echo 'PROCESSING_DATE' >> $path/metadata.txt
        processing_date=$(date +"%Y-%m-%d %H:%M")
        echo $processing_date >> $path/metadata.txt

        if [ $METADATA_LOCATION != "false" ]; then
            echo 'LOCATION' >> $path/metadata.txt
            echo $METADATA_LOCATION >> $path/metadata.txt
        fi

        s3_upload $S3_Bucket$1 $path/metadata.txt $S3_Endpoint $S3_ACCESS_USER $S3_ACCESS_KEY 'metadata.txt'
        if [[ $? -ne 0 ]]; then
            msg_send 'S3 thumbnail upload was not possible.'
        fi
    fi
}

cleanup_after_process () {
    #Check if pictures should be deleted
    if [ $PICS_DELETE == 'true' ]; then
        cat $path/tmpfiles2.txt | xargs rm
    fi
    rm -r $path/pics/thumb.jpg
    rm $path/tmpout.avi
    rm $path/tmpout.mp3
    rm -r $path/tmpfiles*
    rm $path/thumb.jpg
    if [ $OUT_VIDEO_DELETE == 'true' ]; then
        rm $path/out.mp4
    fi
    rm $path/metadata.txt
}


generate_music
generate_images_to_process
startdate=$(generate_date_start)
enddate=$(generate_date_end)
datetxt=$(generate_date_text)
generate_thumbnail
generate_video

#Generate folder for upload
new_upload_folder=$(date +"/%Y/%m/%d")

#Upload
upload_video $new_upload_folder
upload_thumbnail $new_upload_folder
upload_metadata $new_upload_folder

cleanup_after_process
