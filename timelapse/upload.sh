#!/bin/bash

s3_upload () {
    bucket=$1
    file=$2
    minio_host=$3
    access_key=$4
    secret=$5
    filename=$6

    host=${S3_HOST:-$minio_host}
    s3_key=${S3_KEY:-$access_key}
    s3_secret=${S3_SECRET:-$secret}

    base_file=$filename
    resource="/${bucket}/${base_file}"
    content_type="application/octet-stream"
    date=`date -R`
    _signature="PUT\n\n${content_type}\n${date}\n${resource}"
    signature=`echo -en ${_signature} | openssl sha1 -hmac ${s3_secret} -binary | base64`

    curl -f -X PUT -T "${file}" -H "Host: $host" -H "Date: ${date}" -H "Content-Type: ${content_type}" -H "Authorization: AWS ${s3_key}:${signature}" https://$host${resource} && echo "SUCCESS!" ||
        return 1
}