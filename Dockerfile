FROM debian:bookworm
RUN apt update
RUN apt install ffmpeg mencoder imagemagick bc curl -y
CMD cd /timelapse && chmod +x /timelapse/gen-vid.sh && /timelapse/gen-vid.sh
